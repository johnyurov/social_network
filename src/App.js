import './App.css';
import Header from './components/Header/Header';
import Nav from './components/Nav/Nav';
import Profile from './components/Profile/Profile';
import Dialogs from './components/Dialogs/Dialogs';
import {BrowserRouter, Route} from 'react-router-dom';


function App(props) {
  return (
    <BrowserRouter>
    <div className="App">
      <Header />
      <Nav />
      <Route path='/me' render={() => <Profile posts={props.posts}/>}/>
      <Route path='/message' render={() => <Dialogs messages={props.messages} dialogs={props.dialogs}/>}/>
      {/* <Route path='/' component={}/>   нам пока не нужно, потому что нечего отрисовывать   */}
    </div>
    </BrowserRouter>
  );
}

export default App;
