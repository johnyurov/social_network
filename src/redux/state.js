  let state = {
      posts: [
        {id: '1', message: 'Hello, World!', likes: '256'},
        {id: '2', message: 'My name is John', likes: '1024'}
      ],
      dialogs: [
        {id: '1', name: 'Tanya' },
        {id: '2', name: 'Natalia'},
        {id: '3', name: 'Sergei' },
        {id: '4', name: 'Sergei' },
        {id: '5', name: 'Vasiliy' }
      ],
      messages: [
        {id: '1', message: 'Hello'},
        {id: '2', message: 'How are you?'},
        {id: '3', message: 'Can we meet today?'},
      ]
  }
export default state;