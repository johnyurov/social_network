import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Nav.module.css';

const MenuItem = (props) => {
  return <div className={styles.item}>
    <NavLink activeClassName={styles.active} to={'/' + props.name}>{props.name}</NavLink>
  </div>
}

const Nav = () => {
  return <nav className={styles.nav}>
    <MenuItem name='me'/>
    <MenuItem name='message'/>
    <MenuItem name='news'/>
    <MenuItem name='friends'/>
    <MenuItem name='information'/>
  </nav>
}

export default Nav;