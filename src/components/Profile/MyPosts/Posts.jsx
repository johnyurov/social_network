import React from 'react';
import Post from './Post/Post';
import styles from './Posts.module.css';



const Posts = (props) => {
  let postsElements = props.posts.map(p => <Post message={p.message} likes={p.likes}/>)
    return <div>
      { postsElements }
  </div>
}

export default Posts;