import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './../Dialogs.module.css';

const DialogItems = (props) => {
   return <div className={styles.dialog + ' ' + styles.active}>
        <NavLink to={'/message/' + props.id}>{props.name}</NavLink>
    </div>
}

export default DialogItems;