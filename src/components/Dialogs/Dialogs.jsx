import React from 'react';
import styles from './Dialogs.module.css';
import DialogItems from './DialogItems/DialogItems';
import Message from './Message/Message';



const Dialogs = (props) => {
    // map всегда вызывается у массива и возвращет другой массив, который мы присваеваем другой переменной 
    let dialogsElements = props.dialogs.map(d => <DialogItems name={d.name} id={d.id} />)//передаем в map стрелочную функцию и эта    
                                                                                   //функция вызовется столько раз, сколько у нас //элементов в исходном массиве и каждый раз 
                                                                                   //функция map засунет в эту функцию конкретный //элемент из исходного массива 
    let messagesElements = props.messages.map(m => {//Второй вариант использования map, более длинный, чем первый
        return <Message message={m.message} />})
    return <div className='content'>
        <div className={styles.dialogs}>
            <div className={styles.dialogItems}>
                { dialogsElements }
            </div>
            <div className={styles.messages}>
                { messagesElements }
            </div>
        </div>
    </div>
}

export default Dialogs;